{
  description = "A Swarm of Ninja Snakes";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    # systems.url = "github:nix-systems/default-linux"; # for boch arm & x86
    systems.url = "github:nix-systems/x86_64-linux";
    treefmt-nix.url = "github:numtide/treefmt-nix";
    process-compose-flake.url = "github:Platonic-Systems/process-compose-flake";
    services-flake.url = "github:juspay/services-flake";
    devenv.url = "github:cachix/devenv";
    zig2nix.url = "github:Cloudef/zig2nix";

    northwind.url = "github:pthom/northwind_psql";
    northwind.flake = false;
  };
  outputs = inputs:
    inputs.flake-parts.lib.mkFlake { inherit inputs; } {
      systems = import inputs.systems;
      imports = [
        ./hydraJobs.nix
        inputs.treefmt-nix.flakeModule
        inputs.devenv.flakeModule
      ];
      perSystem = { self', system, pkgs, lib, ... }:
        let
          env = inputs.zig2nix.outputs.zig-env.${system} { };
          system-triple = env.lib.zigTripleFromString system;
        in rec {
          # nix run . 
          apps.default = env.app [ ] ''zig build run -- "$@"'';

          # nix run .#build
          apps.build = env.app [ ] ''zig build "$@"'';

          # nix run .#test
          apps.test = env.app [ ] ''zig build test -- "$@"'';

          # nix run .#docs
          apps.docs = env.app [ ] ''zig build docs -- "$@"'';

          # nix run .#deps
          apps.deps = env.showExternalDeps;

          packages.target = env.pkgs.lib.genAttrs env.lib.allTargetTriples
            (target:
              env.packageForTarget target ({
                src = env.pkgs.lib.cleanSource ./.;

                nativeBuildInputs = with env.pkgs; [ ];
                buildInputs = with env.pkgsForTarget target; [ ];

                # Smaller binaries and avoids shipping glibc.
                zigPreferMusl = true;

                # This disables LD_LIBRARY_PATH mangling, binary patching etc...
                # The package won't be usable inside nix.
                zigDisableWrap = true;
              }));

          packages.default = packages.target.${system-triple}.override {
            # Prefer nix friendly settings.
            # zigPreferMusl = true;
            zigDisableWrap = false;
          };
          hydraJobs.main = packages.default;
          hydraJobs.test = pkgs.hello;

          treefmt.programs = {
            zig.enable = true;
            nixfmt.enable = true;
          };

          devenv.shells.default = {
            languages = {
              zig.enable = true;
              nix.enable = true;
            };
            env = { };
            process.implementation = "overmind";
            # services.redis = {
            # enable = true;
            # bind = "0.0.0.0";
            # };
            processes = {
              TCR.exec = ''
                while true
                do
                  inotifywait -r -e modify ./src/
                  zig build test && 
                  (
                  printf '\n';
                  tput setaf 2;
                  printf '==%.0s' {1..9}; 
                  printf '  COMMITED  ';
                  printf '==%.0s' {1..9}; 
                  printf '\n';
                  tput dim;
                  result=$(git commit -am working)
                  echo $result
                  tput setaf 2;
                  printf '==%.0s' {1..24}; 
                  printf '\n';
                 ) || (
                  tput bel;
                  printf '\n';
                  tput setaf 1;
                  printf '==%.0s' {1..9}; 
                  printf '  REVERTED  ';
                  printf '==%.0s' {1..9}; 
                  printf '\n';
                  tput dim;
                  git reset --hard
                  tput setaf 1;
                  printf '==%.0s' {1..24};
                  printf '\n';
                 )
                done
              '';
              Sync.exec = ''
                while true
                do
                  git pull --rebase
                  git push origin main
                done
              '';
            };
            packages = with pkgs; [
              dig
              openssl

              valgrind
              gprof2dot
              graphviz
              simple-http-server
              inotify-tools

              nixfmt
            ];
          };
        };
    };
}
